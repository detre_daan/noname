﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMain : MonoBehaviour
{
    EnemyStats enemyStats;

    void Start()
    {
        enemyStats = GetComponent<EnemyStats>();
    }

    //Checks for a trigger and if its a player decrease the player health based on enemy stats
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerStatusClass>())
        {
            collision.GetComponent<PlayerStatusClass>().Hitpoints = -enemyStats.EnemyDamage;
        }

        if (collision.GetComponent<ToolStats>())
        {
            enemyStats.EnemyHitpoints = -collision.GetComponent<ToolStats>().ToolDamage;
        }
    }
}
