﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    #region Variables
    [SerializeField] float enemyHitPoints = 50f;
    [SerializeField] float enemyDamage = 10f;
    #endregion

    public float EnemyDamage
    {
        get { return enemyDamage; }
        set { enemyDamage += value; }
    }

    //Sets enemy hitpoints, if health is 0 or lower destroy enemy
    public float EnemyHitpoints
    {
        get { return enemyHitPoints; }
        set { 
            enemyHitPoints += value;
            if (enemyHitPoints <= 0)
            {
                FindObjectOfType<AudioObject>().ZombieDeath();
                Destroy(gameObject);
            }
        }
    }
}
