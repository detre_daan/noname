﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;

public class EnemyPathingMain : MonoBehaviour
{
    #region Variables
    [SerializeField] Transform target = null;
    [SerializeField] float speed = 25f;
    [SerializeField] float nextWaypointDistance = 1.3f;
    [SerializeField] Transform enemyGraphics = null;

    int currentWaypoint = 0;
    
    Path path;
    PauseMenu pauseMenu;
    Seeker seeker;
    Rigidbody2D rb;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        pauseMenu = FindObjectOfType<PauseMenu>().GetComponent<PauseMenu>();
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        //Repeat function, start cooldown, repeatrate
        InvokeRepeating("UpdatePath", 0f, .5f);
    }

    void UpdatePath()
    {
        if (target != null)
        {
            if (seeker.IsDone())
            {
                //Creates path for with checkpoints for the enemy to move towards
                seeker.StartPath(rb.position, target.position, OnPathComplete);
            }

        }

    }

    void OnPathComplete(Path p)
    {
        //gives error when p can't be completed
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    void FixedUpdate()
    {
        if (!pauseMenu.GameIsPaused)
        {
            MoveEnemyTonextPoint();
        }
    }

    //Looks for the next point to move towards and pushes enemy towards next point
    private void MoveEnemyTonextPoint()
    {
        if (path == null)
            return;

        //Checks if there are more waypoints in the path (end of path)
        if (currentWaypoint >= path.vectorPath.Count)
        {
            return;
        }

        // Takes position of current waypoint minus the current position giving a vector from
        // your position to the next waypoint and normalize it to ensure the length is 1
        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;

        rb.AddForce(force);
        
        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

        RotateEnemyToDirection();

        //move to the next waypoint if the waypoint is reached
        if (distance < nextWaypointDistance)
        {
            currentWaypoint++;
        }

    }    

    //Rotate enemy in direction of force vector
    private void RotateEnemyToDirection()
    {
        if (rb.velocity.x >= 0.01f)
            enemyGraphics.localScale = new Vector3(-1, 1, 1);
        else if (rb.velocity.x <= -0.01f)
            enemyGraphics.localScale = new Vector3(1, 1, 1);
    }
}
