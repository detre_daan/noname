﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcText : MonoBehaviour
{
    #region Variables
    [SerializeField] GameObject text = null;
    #endregion

    //Checks if player is withing range to read text and enables/disables it Based on that
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerMainScript>())
        {
            EnableText();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerMainScript>())
        {
            DisableText();
        }
    }
    void DisableText()
    {
        text.SetActive(false);
    }
    void EnableText()
    {
        text.SetActive(true);
    }
}
