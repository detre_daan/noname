﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionInteract : MonoBehaviour
{
    [HideInInspector]
    public bool followsPlayer = false;

    Animator animator;

    private void Start()
    {
        animator = transform.parent.GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        setInteractable(true, collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        setInteractable(false, collision);
    }

    void setInteractable(bool onOf, Collider2D collision)
    {
        if (collision.GetComponent<PlayerMainScript>() && !followsPlayer)
        {
            animator.SetBool("Interactable", onOf);        
        }            
    }
    void setInteractable(bool onOf)
    {
        animator.SetBool("Interactable", onOf);
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerMainScript>())
        {
            if (Input.GetButton("Interact"))
            {
                setInteractable(false);
                followsPlayer = true;
            }
            else
                return;
        }

    }
    private void Update()
    {
        if (followsPlayer && Input.GetButton("UnInteract"))
        {
            transform.parent.GetComponent<InsideBasementMovement>().ReturnToDefaultPathing();
            followsPlayer = false;
        }
    }
}
