﻿using Cinemachine.Utility;
using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class InsideBasementMovement : MonoBehaviour
{
    #region Variables
    [SerializeField] float speed = 25f;
    [SerializeField] float nextWaypointDistance = 1.3f;
    [SerializeField] Transform enemyGraphics = null;
    [SerializeField] int patrolRandomness = 10;
    [SerializeField] float minionBrokenCheckMain = 1f;

    bool goingLeft = true;
    int currentWaypoint = 0;
    float minionDistance = 0;
    float minionMinDistance = 2f;
    float minionMaxDistance = 5f;
    float currentMoveSpeed = 0;
    float minionBrokenCheck = 0;

    BuildingContainer buildingContainer = null;
    Transform target = null;
    Path path;
    PauseMenu pauseMenu;
    Seeker seeker;
    Rigidbody2D rb;
    MinionInteract minionInteract;
    GameObject player;
    #endregion

    // Minions will have a more in depth pathfdinding compared to Enemy's this is why I won't use the
    // Same script for enemy's
    void Start()
    {
        SetUpVariables();
    }

    private void SetUpVariables()
    {
        minionBrokenCheck = minionBrokenCheckMain;
        player = GameObject.Find("Player");
        minionInteract = GetComponentInChildren<MinionInteract>();
        buildingContainer = FindObjectOfType<BuildingContainer>().GetComponent<BuildingContainer>();
        buildingContainer.UpdateBuildings();
        pauseMenu = FindObjectOfType<PauseMenu>().GetComponent<PauseMenu>();
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();
        target = buildingContainer.GetMinX();
        InvokeRepeating("UpdatePath", 0f, .5f);
    }

    void UpdatePath()
    {
        if (target != null)
        {
            if (minionInteract.followsPlayer)
            {
                if (minionDistance == 0)
                {
                    Random.InitState(System.DateTime.Now.Millisecond);
                    minionDistance = Random.Range(minionMinDistance, minionMaxDistance);
                }
                target = player.transform;
            }
            if (seeker.IsDone())
                //Creates path for with checkpoints for the enemy to move towards
                seeker.StartPath(rb.position, target.position, OnPathComplete);      
        }
    }

    public void ReturnToDefaultPathing()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        int rnd = Random.Range(0, 1);

        if (rnd == 0)
        {
            target = buildingContainer.GetMaxX();

        }
        else
        {
            target = buildingContainer.GetMinX();
        }
    }

    void OnPathComplete(Path p)
    {
        //gives error when p can't be completed
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    void FixedUpdate()
    {
        minionBrokenCheck -= Time.deltaTime;
        currentMoveSpeed = rb.velocity.magnitude;

        if (currentMoveSpeed < 0.1 && minionBrokenCheck < 0 && !minionInteract.followsPlayer)
        {
            Debug.Log("error in minion pathing");
            ChangeDirection();

            minionBrokenCheck = minionBrokenCheckMain;
        }

        if (!pauseMenu.GameIsPaused && 
            Vector3.Distance(target.position, transform.position) > minionDistance 
            | !minionInteract.followsPlayer)
        {
                MoveEnemyTonextPoint();
        }
        CheckForMinionPatrolFinish();

    }

    private void CheckForMinionPatrolFinish()
    {
        if (!minionInteract.followsPlayer)
        {
            if (transform.position.x + 0.2f >= buildingContainer.GetMaxX().position.x)
            {
                goingLeft = true;
                target = buildingContainer.GetMinX();
            }
            else if (transform.position.x - 0.2f<= buildingContainer.GetMinX().position.x)
            {
                goingLeft = false;
                target = buildingContainer.GetMaxX();

            }
        }

    }

    //Looks for the next point to move towards and pushes minion towards next point
    private void MoveEnemyTonextPoint()
    {
        if (path == null)
            return;

        //Checks if there are more waypoints in the path (end of path)
        if (currentWaypoint >= path.vectorPath.Count)
        {
            return;
        }

        // Takes position of current waypoint minus the current position giving a vector from
        // your position to the next waypoint and normalize it to ensure the length is 1
        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;

        rb.AddForce(force);

        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

        RotateMinionToDirection();

        //move to the next waypoint if the waypoint is reached
        if (distance < nextWaypointDistance)
        {
            Random.InitState(System.DateTime.Now.Millisecond);
            int rnd = Random.Range(0, patrolRandomness);

            if (rnd == 0)
            {
                ChangeDirection();
            }

            currentWaypoint++;
        }

    }

    private void ChangeDirection()
    {
        if (goingLeft)
        {
            target = buildingContainer.GetMinX();
            goingLeft = false;
        }
        else if (!goingLeft)
        {
            target = buildingContainer.GetMaxX();
            goingLeft = true;
        }
    }

    //Rotate enemy in direction of force vector
    private void RotateMinionToDirection()
    {
        if (rb.velocity.x >= 0.01f)
        {
            enemyGraphics.localScale = new Vector3(-1, 1, 1);

        }
        else if (rb.velocity.x <= -0.01f)
        {
            enemyGraphics.localScale = new Vector3(1, 1, 1);
        }
    }
}
