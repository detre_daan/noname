﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarMain : MonoBehaviour
{
    #region Variables
    [SerializeField] Gradient gradient = null;
    [SerializeField] Image fill = null;
    Slider slider;
    #endregion

    void Start()
    {
        
        slider = GetComponent<Slider>();
    }

    public void SetMaxHealth(float health)
    {
        slider.maxValue = health;
    }

    //Sets the healthbar value based on the players health
    public void SetHealth(float health)
    {
        slider.value = health;

        fill.color = gradient.Evaluate(slider.normalizedValue);
        
    }
}
