﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatusClass : MonoBehaviour
{
    #region variables
    // health indicators
    [SerializeField] float hitpoints = 0;
    [SerializeField] float hungerLevel = 0;
    [SerializeField] float thirstLevel = 0;
    [SerializeField] float bodyWarmth = 0;

    // resource indicators
    [SerializeField] float wood = 0;
    [SerializeField] float stone = 0;
    [SerializeField] float iron = 0;
    [SerializeField] float diamond = 0;
    #endregion

    public void AddAllValues(float hitpoints, float hungerLevel, float thirstLevel, float bodyWarmth, float wood, float stone, float iron, float diamond)
    {
        this.hitpoints = hitpoints;
        this.hungerLevel = hungerLevel;
        this.thirstLevel = thirstLevel;
        this.bodyWarmth = bodyWarmth;

        this.wood = wood;
        this.stone = stone;
        this.iron = iron;
        this.diamond = diamond;
    }

    public string ResourceSaveLine()
    {
        return $"{hitpoints};{hungerLevel};{thirstLevel};{bodyWarmth};{wood};{stone};{iron};{diamond}";
    }

    public float Hitpoints
    {
        get { return hitpoints; }
        set {
            hitpoints += value;
            FindObjectOfType<AudioObject>().PlayerDamage();
            if (hitpoints <= 0)
            {
                FindObjectOfType<LevelLoader>().LoadGameOver();
                Destroy(gameObject);
            }
        }
    }

    public float Hunger
    {
        get{ return hungerLevel; }
        set {hungerLevel += value; }
    }

    public float Thirst
    {
        get { return thirstLevel; }
        set { thirstLevel += value; }
    }

    public float BodyWarmth
    {
        get { return bodyWarmth; }
        set { bodyWarmth += value; }
    }

    public float Wood
    {
        get { return wood; }
        set { wood += value; }
    }

    public float Stone
    {
        get { return stone; }
        set { stone += value;  }
    }

    public float Iron
    {
        get { return iron; }
        set { iron += value; }
    }

    public float Diamond
    {
        get { return diamond; }
        set { diamond += value; }
    }
}
