﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetAndChangeActiveTool : MonoBehaviour
{
    #region Variables
    public int toolUsingIndex = 0;

    [SerializeField] float mainCooldownSpeed = 0.15f;
    [SerializeField] int maxToolAmount = 2;
    [SerializeField] Transform tools = null;

    BuildingUi buildingUi = null;
    Animator toolAnimator;
    ToolStats toolStats;
    List<Transform> toolHolders = new List<Transform>();

    float toolCooldown = 0;
    float toolCooldownBase = 1;
    float cooldown = 0;
    #endregion

    void Start()
    {
        SetupVariables();
    }

    private void SetupVariables()
    {
        foreach (Transform child in tools)
        {
            toolHolders.Add(child);
        }

        buildingUi = FindObjectOfType<BuildingUi>().GetComponent<BuildingUi>();
        cooldown = mainCooldownSpeed;
        FindToolObjectInPlayer();
        toolCooldownBase = toolStats.ToolCooldown;
    }

    void Update()
    {
        GetInputForToolChange();
    }

    private void GetInputForToolChange()
    {
        cooldown -= Time.deltaTime;

        if (cooldown < 0)
        {
            if (Input.GetButton("QuickMoveItemLeft") && !buildingUi.uiOpen)
            {
                toolUsingIndex--;
                ChangeActiveTool(true);

            }
            else if (Input.GetButton("QuickMoveItemRight") && !buildingUi.uiOpen)
            {
                toolUsingIndex++;
                ChangeActiveTool(false);
            }

            cooldown = mainCooldownSpeed;
        }
    }

    private void ChangeActiveTool(bool left)
    {
        if (left)
        {
            if (toolUsingIndex < 0)
            {
                toolUsingIndex = maxToolAmount;
            }
        }
        else
        {
            if (toolUsingIndex > maxToolAmount)
            {
                toolUsingIndex = 0;
            }
        }

        foreach (Transform tool in toolHolders)
        {

            if (tool.GetComponent<ToolHolder>().GetIndex == toolUsingIndex)
            {
                tool.GetComponent<ToolHolder>().SetToolActive();
                toolStats = tool.GetComponent<ToolStats>();
            }
            else
                tool.GetComponent<ToolHolder>().SetToolInactive();
        }
    }

    private void FindToolObjectInPlayer()
    {
        toolStats = GetComponentInChildren<ToolStats>();
        toolAnimator = GetComponentInChildren<Animator>();
    }
    public void PlayerUseTool()
    {
        toolCooldown -= Time.deltaTime;

        if (Input.GetButton("Fire1") && toolCooldown <= 0 && !buildingUi.uiOpen)
        {
            FindToolObjectInPlayer();
            toolAnimator.SetTrigger("hit");
            toolCooldown = toolCooldownBase;
        }
    }
}
