﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMainScript : MonoBehaviour
{
    #region variables
    [SerializeField] float speed = 15f;
    [SerializeField] GameObject healthBar = null;

    public Transform buildPos = null;

    HealthBarMain healthBarMain;
    PlayerStatusClass playerStatusClass;
    GetAndChangeActiveTool getAndChangeActiveTool;
    #endregion

    void Start()
    {
        healthBarMain = healthBar.GetComponent<HealthBarMain>();
        playerStatusClass = GetComponent<PlayerStatusClass>();
        getAndChangeActiveTool = GetComponent<GetAndChangeActiveTool>();
    }

    void Update()
    {
        UpdateHealthBar();
        if (!FindObjectOfType<PauseMenu>().GetComponent<PauseMenu>().GameIsPaused)
        {
            MovePlayer();        
            getAndChangeActiveTool.PlayerUseTool();
        }

    }

    void MovePlayer()
    {
        transform.Translate(Input.GetAxis("Horizontal") * speed * Time.deltaTime, 0f, 0f);

        Vector3 characterScale = transform.localScale;
        if(Input.GetAxis("Horizontal") < 0)
        {
            characterScale.x = 1;
        }
        else if(Input.GetAxis("Horizontal") > 0)
        {
            characterScale.x = -1;
        }

        transform.localScale = characterScale;
    
    }

    void UpdateHealthBar()
    {
        healthBarMain.SetHealth(playerStatusClass.Hitpoints);
    }
}
