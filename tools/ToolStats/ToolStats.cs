﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolStats : MonoBehaviour
{
    #region Variables
    [Header("ToolSpecs")]
    [SerializeField] float toolDamage = 25f;
    [SerializeField] float toolCooldown = 1f;

    [Header("ToolType")]
    [SerializeField] bool sword = true;
    [SerializeField] bool axe = false;
    [SerializeField] bool pickaxe = false;
    #endregion

    public float ToolDamage
    {
        get { return toolDamage; }
    }

    public float ToolCooldown
    {
        get { return toolCooldown; }
    }

    public bool IsSword
    {
        get { return sword; }
    }

    public bool IsAxe
    {
        get { return axe; }
    }

    public bool IsPickaxe
    {
        get { return pickaxe; }
    }
}
