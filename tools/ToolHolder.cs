﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolHolder : MonoBehaviour
{
    [SerializeField] int IndexNr = 0;

    public int GetIndex
    {
        get { return IndexNr; }
    }

    public void SetToolInactive()
    {
        gameObject.SetActive(false);
    }

    public void SetToolActive()
    {
        gameObject.SetActive(true);
    }
}
