﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    #region Variables
    [SerializeField] new Animator animation = null;
    [SerializeField] float transition = 1f;
    #endregion

    public void LoadNextScene()
    {
        StartCoroutine(LoadLevelDelay(SceneManager.GetActiveScene().buildIndex + 1));
    }

    public void LoadGameOver()
    {
        StartCoroutine(LoadLevelDelay("Game Over"));
    }

    public void LoadIndex(int index)
    {
        StartCoroutine(LoadLevelDelay(index));
    }

    //Using coroutines to have a nice delay to put a simple game transition in place
    IEnumerator LoadLevelDelay(int levelIndex)
    {
        animation.SetTrigger("start");

        yield return new WaitForSeconds(transition);

        SceneManager.LoadScene(levelIndex);      
    }

    IEnumerator LoadLevelDelay(string LevelName)
    {
        animation.SetTrigger("start");

        yield return new WaitForSeconds(transition);

        SceneManager.LoadScene(LevelName);
    }
}
