﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateOrDoor : MonoBehaviour
{
    [SerializeField] Transform teleportTo = null;
    [SerializeField] BackgroundMain[] backgroundMains = null;

    private void Start()
    {
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetAxis("Vertical") > 0 && collision.GetComponent<PlayerMainScript>())
        {
            Debug.Log("hi");
            collision.transform.position = teleportTo.transform.position;

            foreach (var background in backgroundMains)
            {
                background.HardMoveBackgroundToPlayer();
            }
        }
    }
}
