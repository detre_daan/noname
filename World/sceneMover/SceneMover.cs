﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMover : MonoBehaviour
{
    #region Variables
    [SerializeField] string sceneName = "";
    #endregion

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerMainScript>())
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
