﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMain : MonoBehaviour
{
    #region Variables
    [SerializeField] float scrollSpeed = 1f;
    
    Material material;
    Vector2 offSet;
    #endregion

    private void Start()
    {
        material = GetComponent<Renderer>().material;
        offSet = new Vector2(scrollSpeed * Time.deltaTime, 0);
    }

    void Update()
    {
        MoveBasedOnPlayerMovement();
    }

    private void MoveBasedOnPlayerMovement()
    {
        gameObject.transform.position = new Vector3(GameObject.Find("Player").transform.position.x,
            transform.position.y, transform.position.z);

        if (Input.GetAxis("Horizontal") < 0)
        {
            material.mainTextureOffset -= offSet * Time.deltaTime;
        }
        else if (Input.GetAxis("Horizontal") > 0)
        {
            material.mainTextureOffset += offSet * Time.deltaTime;
        }
    }

    public void HardMoveBackgroundToPlayer()
    {
        gameObject.transform.position = new Vector3(GameObject.Find("Player").transform.position.x,
            GameObject.Find("Player").transform.position.y, transform.position.z);
    }
}
