﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldStatClass : MonoBehaviour
{
    #region Variables
    [SerializeField] float warmthPerSecond = -1f;
    [SerializeField] float thirstPerSecond = -0.2f;
    [SerializeField] float hungerPerSecond = -0.1f;
    [SerializeField] bool allowedToBuildInWorld = true;
    #endregion

    public float WarmthIndicator
    {
        get { return warmthPerSecond; }
        set { warmthPerSecond = value; }
    }

    public float ThirstIndicator
    {
        get { return thirstPerSecond; }
        set { thirstPerSecond = value; }
    }

    public float HungerIndicator
    {
        get { return hungerPerSecond; }
        set { hungerPerSecond = value; }
    }

    public bool GetAllowedToBuildInWorld
    {
        get { return allowedToBuildInWorld; }
        set { allowedToBuildInWorld = value; }
    }
}
