﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WorldMainClass : MonoBehaviour
{
    #region variables
    GameObject player;
    PlayerStatusClass playerStatus;
    WorldStatClass worldStat;
    
    float second = 1f;
    #endregion

    void Start()
    {
        player = GameObject.Find("Player");
        playerStatus = player.GetComponent<PlayerStatusClass>();
        worldStat = GetComponent<WorldStatClass>();
    }

    void Update()
    {
        ChangeIndicator();
    }

    private void ChangeIndicator()
    {
        second -= Time.deltaTime;
        if (second < 0)
        {
            playerStatus.Thirst = worldStat.ThirstIndicator;
            playerStatus.BodyWarmth = worldStat.WarmthIndicator;
            playerStatus.Hunger = worldStat.HungerIndicator;
            second = 1f;
        }
    }
}
