﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceGainMain : MonoBehaviour
{
    #region variables
    [SerializeField] int requiredHitsToBreak = 3;
    [SerializeField] int amountGainOnHit = 25;

    [SerializeField] bool wood = true;
    [SerializeField] bool stone = false;
    [SerializeField] bool iron = false;
    [SerializeField] bool diamond = false;

    AudioObject audioObject;
    Transform player = null;
    PlayerStatusClass playerStatus = null;
    #endregion

    private void Start()
    {
        audioObject = FindObjectOfType<AudioObject>().GetComponent<AudioObject>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ResourceBehavourOnCollision(collision);
    }

    private void ResourceBehavourOnCollision(Collider2D collision)
    {
        // Checks if player collides with trigget
        if (collision.GetComponent<ToolStats>())
        {
            if (playerStatus == null)
            {
                player = collision.transform.parent.transform.parent.transform.parent;
                playerStatus = player.GetComponent<PlayerStatusClass>();
            }



            CheckIfCanBreakAndGiveAmountGainOnHit(collision);
        }
    }

    private void CheckIfCanBreakAndGiveAmountGainOnHit(Collider2D collision)
    {
        if (wood && collision.GetComponent<ToolStats>().IsAxe)
        {
            audioObject.WoodHit();
            playerStatus.Wood = amountGainOnHit;
            player.GetComponent<PlayerMessage>().TriggerMessage($"You got +{amountGainOnHit} wood");
            requiredHitsToBreak--;
        }

        else if (stone && collision.GetComponent<ToolStats>().IsPickaxe)
        {
            audioObject.StoneHit();
            playerStatus.Stone = amountGainOnHit;
            player.GetComponent<PlayerMessage>().TriggerMessage($"You got +{amountGainOnHit} stone");
            requiredHitsToBreak--;
        }

        else if (iron && collision.GetComponent<ToolStats>().IsPickaxe)
        {
            audioObject.IronHit();
            playerStatus.Iron = amountGainOnHit;
            player.GetComponent<PlayerMessage>().TriggerMessage($"You got +{amountGainOnHit} iron");
            requiredHitsToBreak--;
        }

        else if (diamond && collision.GetComponent<ToolStats>().IsPickaxe)
        {
            audioObject.DiamondHit();
            playerStatus.Diamond = amountGainOnHit;
            player.GetComponent<PlayerMessage>().TriggerMessage($"You got +{amountGainOnHit} diamond");
            requiredHitsToBreak--;
        }
        else
        {
            player.GetComponent<PlayerMessage>().TriggerMessage("You'll need another tool for this!");
        }
        
        if (requiredHitsToBreak <= 0)
        {
            Destroy(gameObject);

        }    
    }
}
