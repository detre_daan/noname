﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildStats : MonoBehaviour
{
    #region Variables
    [Header("BuildingCost")]
    [SerializeField] float woodCost = 150;
    [SerializeField] float stoneCost = 0;
    [SerializeField] float ironCost = 0;
    [SerializeField] float diamondCost = 0;
    #endregion

    public float GetWoodCost
    {
        get { return woodCost; }
    }

    public float GetStoneCost
    {
        get { return stoneCost; }
    }

    public float GetIronCost
    {
        get { return ironCost; }
    }

    public float GetDiamondCost
    {
        get { return diamondCost; }
    }
}
