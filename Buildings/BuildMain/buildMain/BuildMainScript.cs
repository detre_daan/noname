﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildMainScript : MonoBehaviour
{
    #region Variables
    [SerializeField] bool placed = false;

    GameObject player;
    PlayerStatusClass playerStatus;
    BuildStats buildStats;
    #endregion

    private void Start()
    {
        player = GameObject.Find("Player");
        playerStatus = player.GetComponent<PlayerStatusClass>();
        buildStats = GetComponent<BuildStats>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        //Checks if you try to build your build against another building
        if (collision.transform.tag == "build" && !placed)
        {
            playerStatus.Wood = buildStats.GetWoodCost;
            playerStatus.Stone = buildStats.GetStoneCost;
            playerStatus.Iron = buildStats.GetIronCost;
            playerStatus.Diamond = buildStats.GetDiamondCost;

            player.GetComponent<PlayerMessage>().TriggerMessage("You can't place a building here!");

            Destroy(gameObject);
        }
    }

    public void SetPlacedTrue()
    {
        StartCoroutine(PlacedCooldown());
    }

    IEnumerator PlacedCooldown()
    {
        yield return new WaitForSeconds(0.75f);
        placed = true;
    }
}
