﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingContainer : MonoBehaviour
{

    List<Transform> buildings = new List<Transform>();

    void Start()
    {
        UpdateBuildings();
    }

    public void UpdateBuildings()
    {
        buildings.Clear();
        foreach(Transform child in transform)
        {
            buildings.Add(child);
        }
    }

    public Transform GetMaxX()
    {
        UpdateBuildings();

        Transform maxXBuild = null;

        float currentHeighest = 0;
        foreach (Transform build in buildings)
        {
            if (build.position.x >= currentHeighest)
            {
                currentHeighest = build.position.x;
                maxXBuild = build;
            }
        }
        return maxXBuild;
    }

    public Transform GetMinX()
    {
        UpdateBuildings();
        Transform minXBuild = null;
        
        float currentLowest = 0;

        foreach (Transform build in buildings)
        {
            if (build.position.x <= currentLowest)
            {
                currentLowest = build.position.x;
                minXBuild = build;
            }
        }
        return minXBuild;
    }
}
