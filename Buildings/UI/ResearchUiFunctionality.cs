﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ResearchUiFunctionality : MonoBehaviour
{
    #region Variables
    [HideInInspector]
    public int currentline = 1;
    public int currentHeight = 1;    
    
    [SerializeField] GameObject selector = null;
    [SerializeField] float clickCooldownMain = 0.2f;

    GameObject player;
    PlayerStatusClass playerStatusClass = null;
    Selector selectorScript;
    SaveLoadData saveLoadData;
    PlayerMessage playerMessage;
   
    bool[] loadedData;
    float clickCooldown = 0;
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        SetUpVariables();
    }

    private void SetUpVariables()
    {
        clickCooldown = clickCooldownMain;
        player = GameObject.Find("Player");
        playerStatusClass = player.GetComponent<PlayerStatusClass>();
        playerMessage = player.GetComponent<PlayerMessage>();
        saveLoadData = GameObject.Find("Player canvas").GetComponent<SaveLoadData>();
        loadedData = saveLoadData.LoadResearchData();
        gameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        selectorScript = selector.GetComponent<Selector>();
    }

    // Update is called once per frame
    void Update()
    {
        UseUi();
        CloseUi();
    }

    private void UseUi()
    {
        currentline = selectorScript.researchUiCurrentLine;
        currentHeight = selectorScript.researchUiCurrentLineHeight;

        Check2DIndex();
    }

    private void Check2DIndex()
    {
        clickCooldown -= Time.deltaTime;
        if (selectorScript.UiOpen && Input.GetButton("Fire1") && clickCooldown < 0)
        {
            if (currentHeight == 1 && currentline == 1)
            {
                if (!loadedData[0])
                {
                    if (playerStatusClass.Wood >= 1500)
                    {
                        loadedData[0] = true;
                        Debug.Log("going true!");
                        playerStatusClass.Wood = -1500;
                        saveLoadData.SaveResearchData(loadedData);
                        loadedData = saveLoadData.LoadResearchData();
                    }
                    else
                    {
                        playerMessage.TriggerMessage("You don't have enough resources!");
                    }
                }
                else
                {
                    playerMessage.TriggerMessage("You already unlocked this!");
                }
            }
            else if (selectorScript.UiOpen && Input.GetButton("Fire1"))
            {
                Debug.Log("Not hi");
            }
            clickCooldown = clickCooldownMain;
        }
    }

    private void CloseUi()
    {
        if (Input.GetButton("Fire2"))
        {
            Time.timeScale = 1;
            Destroy(gameObject);
        }
    }
}
