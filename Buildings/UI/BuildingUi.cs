﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingUi : MonoBehaviour
{
    #region Variables
    public bool uiOpen = false;

    [SerializeField] GameObject world = null;
    [SerializeField] GameObject buildingChooser = null;
    [SerializeField] GameObject insideRing = null;
    [SerializeField] float mainOpenCooldown = 0.25f;
    [SerializeField] float mainRotationCooldown = 0.1f;
    [SerializeField] float heightAbovePlayer = 3f;
    [SerializeField] int currentInndex = 0;
    [SerializeField] float buildCooldownMain = 1f;

    BuildingContainer buildingContainer = null;
    WorldStatClass worldStatClass;
    BuildStats buildStats;
    PlayerStatusClass playerStatus;
    GameObject player;
    PlayerMainScript playerMainScript;

    List<PlayerBuilds> buildChildren = new List<PlayerBuilds>();

    float rotationCooldown = 0;
    float openCooldown = 0;
    float rotation = 0;
    float buildCooldown = 0;
    #endregion

    private void Start()
    {
        SetUpVariables();
    }

    private void SetUpVariables()
    {
        worldStatClass = world.GetComponent<WorldStatClass>();

        buildingContainer = FindObjectOfType<BuildingContainer>().GetComponent<BuildingContainer>();
        openCooldown = mainOpenCooldown;
        player = GameObject.Find("Player");
        worldStatClass = GameObject.Find("World").GetComponent<WorldStatClass>();
        rotationCooldown = mainRotationCooldown;
        buildCooldown = buildCooldownMain;
        playerStatus = player.GetComponent<PlayerStatusClass>();
        playerMainScript = player.GetComponent<PlayerMainScript>();

        foreach (Transform build in insideRing.transform)
        {
            buildChildren.Add(build.GetComponent<PlayerBuilds>());
        }
    }

    private void Update()
    {
        transform.position = new Vector2(player.transform.position.x, player.transform.position.y + heightAbovePlayer);
        
        rotationCooldown -= Time.deltaTime;
        openCooldown -= Time.deltaTime;

        OpenUi();
        RotateRing();
        PlaceBuild();
    }

    private void PlaceBuild()
    {
        buildCooldown -= Time.deltaTime;

        if (buildCooldown <= 0 && Input.GetButton("Fire1") && worldStatClass.GetAllowedToBuildInWorld && uiOpen)
        {
            buildStats = buildChildren[currentInndex].selectedBuild.GetComponent<BuildStats>();

            if (buildStats.GetWoodCost <= playerStatus.Wood
            && buildStats.GetStoneCost <= playerStatus.Stone && buildStats.GetIronCost <= playerStatus.Iron
            && buildStats.GetDiamondCost <= playerStatus.Diamond)
            {
                playerStatus.Wood = -buildStats.GetWoodCost;
                playerStatus.Stone = -buildStats.GetStoneCost;
                playerStatus.Iron = -buildStats.GetIronCost;
                playerStatus.Diamond = -buildStats.GetDiamondCost;

                GameObject build = Instantiate(buildChildren[currentInndex].selectedBuild,
                    new Vector2(Mathf.Round(playerMainScript.buildPos.position.x),
                    Mathf.Round(playerMainScript.buildPos.position.y)), Quaternion.identity);
                build.transform.parent = GameObject.Find("BuildingContainer").transform;
                build.GetComponent<BuildMainScript>().SetPlacedTrue();
            }
            else if (!worldStatClass.GetAllowedToBuildInWorld)
            {
                player.GetComponent<PlayerMessage>().TriggerMessage("You are not allowed to build here!");
            }
            else
            {
                player.GetComponent<PlayerMessage>().TriggerMessage("You lack the resources to build this!");
            }
            buildCooldown = buildCooldownMain;
            buildingContainer.UpdateBuildings();
        }
    }

    private void RotateRing()
    {
        if (uiOpen && rotationCooldown <= 0)
        {
            rotationCooldown = mainRotationCooldown;
            if (Input.GetButton("QuickMoveItemLeft") && uiOpen)
            {
                currentInndex--;
                rotation += 45;
                insideRing.transform.rotation = Quaternion.Euler(0, 0, rotation);
                
                SetIndex();
            }
            else if (Input.GetButton("QuickMoveItemRight") && uiOpen)
            {
                currentInndex++;
                rotation -= 45;
                insideRing.transform.rotation = Quaternion.Euler(0, 0, rotation);

                SetIndex();            
            }
        }
    }

    private void SetIndex()
    {
        if (currentInndex > 7)
        {
            currentInndex = 0;
        }
        else if (currentInndex < 0)
        {
            currentInndex = 7;
        }
    }

    private void OpenUi()
    {
        if (openCooldown <= 0 && Input.GetButton("Fire2") && worldStatClass.GetAllowedToBuildInWorld)
        {
            if (uiOpen)
            {
                uiOpen = false;
            }
            else
            {
                uiOpen = true;
            }
            buildingChooser.SetActive(uiOpen);

            openCooldown = mainOpenCooldown;
        }
    }
}
