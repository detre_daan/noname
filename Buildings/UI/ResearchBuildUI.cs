﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResearchBuildUI : MonoBehaviour
{
    [SerializeField] GameObject researchBuildUI;
    [SerializeField] float openCloseCooldownMain = 1f;

    GameObject ui;

    float openCloseCooldown = 0;
    bool isOpen = false;
    
    void Start()
    {
        openCloseCooldown = openCloseCooldownMain;

    }

   void Update()
    {
            openCloseCooldown -= Time.deltaTime;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerMainScript>())
        {
            if (Input.GetButton("Interact") && openCloseCooldown <= 0)
            {
                if (!isOpen)
                {
                    ui =  Instantiate(researchBuildUI, GameObject.Find("Player").transform.position, Quaternion.identity);
                    isOpen = true;
                }
                else if (isOpen)
                {
                    Destroy(ui);
                    isOpen = false;
                }

                openCloseCooldown = openCloseCooldownMain;
            }
            else return;
        }

    }
}
