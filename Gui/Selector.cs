﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{
    public int researchUiCurrentLine = 1;
    public int researchUiCurrentLineHeight = 1;    
    public bool UiOpen = true;
    
    [SerializeField] float moveCooldownMain = 0.2f;


    float moveCooldown = 0;

    private void Start()
    {
        moveCooldown = moveCooldownMain;
    }

    void Update()
    {
        ChangeSelector();

    }

    private void ChangeSelector()
    {
        moveCooldown -= Time.deltaTime;
        if (moveCooldown < 0 && UiOpen)
        {
            if (Input.GetAxis("Horizontal") > 0)
            {
                transform.position = new Vector2(transform.position.x + 2.215f, transform.position.y);
                moveCooldown = moveCooldownMain;
                ChangeIndex(false, 1);
            }
            else if (Input.GetAxis("Horizontal") < 0)
            {
                transform.position = new Vector2(transform.position.x - 2.215f, transform.position.y);
                moveCooldown = moveCooldownMain;
                ChangeIndex(false, - 1);
            }

            if (Input.GetAxis("Vertical") > 0)
            {
                transform.position = new Vector2(transform.position.x, transform.position.y + 2.215f);
                moveCooldown = moveCooldownMain;
                ChangeIndex(true, 1);
            }
            if (Input.GetAxis("Vertical") < 0)
            {
                transform.position = new Vector2(transform.position.x, transform.position.y - 2.215f);
                moveCooldown = moveCooldownMain;
                ChangeIndex(true, -1);
            }

        }
    }

    void ChangeIndex(bool goinUp, int index)
    {
        if (!goinUp)
        {
            researchUiCurrentLine += index;
            if (researchUiCurrentLine >= 10)
            {
                researchUiCurrentLine = 1;
                transform.position = new Vector2(transform.position.x - 2.215f * 9, transform.position.y);
            }
            else if (researchUiCurrentLine <= 0)
            {
                researchUiCurrentLine = 9;
                transform.position = new Vector2(transform.position.x + 2.215f * 9, transform.position.y);
            }
        }
        else if (goinUp)
        {
            researchUiCurrentLineHeight += index;
            if (researchUiCurrentLineHeight >= 6)
            {
                researchUiCurrentLineHeight = 1;
                transform.position = new Vector2(transform.position.x , transform.position.y - 2.215f * 5);
            }
            else if (researchUiCurrentLineHeight <= 0)
            {
                researchUiCurrentLineHeight = 5;
                transform.position = new Vector2(transform.position.x , transform.position.y + 2.215f * 5);
            }
        }
    }
}
