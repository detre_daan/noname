﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class optionsControls : MonoBehaviour
{
    #region Variables
    [SerializeField] bool optionsOpen = false;
    [SerializeField] int currentIndex;
    [SerializeField] int maxBtnIndex;
    [SerializeField] GameObject menu;
    #endregion

    public bool GetOptionsOpen
    {
        get { return optionsOpen; }
        set { optionsOpen = value; }
    }

    void Update()
    {
        CloseOptions();
    }

    private void CloseOptions()
    {
        if (optionsOpen && Input.GetButton("Fire2"))
        {
            gameObject.SetActive(false);
        }
    }
}
