﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonController : MonoBehaviour
{
    #region Variables
    public int index;
    public AudioSource clickAudio;

    [SerializeField] bool keyDown;
    [SerializeField] int maxIndex = 0;
    [SerializeField] float cooldownBase = 0.15f;

    float cooldown = 0;
    #endregion

    void Start()
    {
        clickAudio = GetComponent<AudioSource>();
        cooldown = cooldownBase;
    }

    void Update()
    {
        CheckKeyInput();
    }

    private void CheckKeyInput()
    {
        // Changes the index which indicates which button has to be animated & used when clicked
        if (!keyDown)
        {
            cooldown -= Time.unscaledDeltaTime;
            if (cooldown < 0)
            {
                if (Input.GetAxis("Vertical") < 0)
                {
                    if (index < maxIndex)
                    {
                        index++;
                    }
                    else
                    {
                        index = 0;
                    }
                    cooldown = cooldownBase;
                }
                else if (Input.GetAxis("Vertical") > 0)
                {
                    if (index > 0)
                    {
                        index--;
                    }
                    else
                    {
                        index = maxIndex;
                    }
                    cooldown = cooldownBase;
            }

        }
            keyDown = true;
        }
        else
        {
            keyDown = false;
        }
    }
}
