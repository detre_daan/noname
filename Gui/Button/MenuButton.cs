﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    #region Variables
    [Header("Button function")]
    [SerializeField] bool toMainMenu = false;
    [SerializeField] bool openOptions = false;
    [SerializeField] bool resumeGame = false;
    [SerializeField] bool nextScene = false;
    [SerializeField] bool openCredits = false;
    [SerializeField] bool loadGame = false;
    [SerializeField] bool exitGame = false;
    [SerializeField] bool saveGame = false;
    [SerializeField] GameObject optionsCanvas = null;
    [SerializeField] PauseMenu pauseMenu = null;

    [Header("Button index")]
    [SerializeField] int thisIndex = 0;

    AnimatorFunctions animatorFunctions;
    Animator animator;
    [SerializeField] MenuButtonController menuButtonController;
    LevelLoader levelLoader;

    #endregion


    private void Start()
    {
        SetupVariables();
    }

    private void SetupVariables()
    {
        menuButtonController = transform.parent.GetComponent<MenuButtonController>();
        animator = GetComponent<Animator>();
        animatorFunctions = GetComponent<AnimatorFunctions>();
        levelLoader = FindObjectOfType<LevelLoader>().GetComponent<LevelLoader>();
    }

    void Update()
    {
        ChangeAnimationAndExecuteOnClick();
    }

    private void ChangeAnimationAndExecuteOnClick()
    {
        if (menuButtonController.index == thisIndex)
        {
            // Checks what type of button it is and executes needed code when 
            // pressing Fire1 or A on your controller
            animator.SetBool("selected", true);
            if (Input.GetButton("Fire1"))
            {
                if (toMainMenu)
                {
                    levelLoader.LoadIndex(0);
                }
                else if (openOptions)
                {
                    pauseMenu.SetPauseMenuInactive();
                    optionsCanvas.SetActive(true);
                    pauseMenu.GameIsPaused = true;
                }
                else if (resumeGame)
                {
                    transform.parent.transform.parent.GetComponent<PauseMenu>().Resume();
                }
                else if (nextScene)
                {
                    levelLoader.LoadNextScene();
                }
                else if (openCredits)
                {
                    throw new NotImplementedException();
                }
                else if (loadGame)
                {
                    throw new NotImplementedException();
                }
                else if (exitGame)
                {
                    throw new NotImplementedException();
                }
                else if (saveGame)
                {
                    FindObjectOfType<LevelLoader>().GetComponent<SaveLoadData>().SaveResourceData();
                }
                else
                {
                    return;
                }
                Time.timeScale = 1f;
                animator.SetBool("pressed", true);
            }
        }
        else if (animator.GetBool("pressed"))
        {
            animator.SetBool("pressed", false);
            animatorFunctions.disableOnce = true;
        }
        else
        {
            animator.SetBool("selected", false);
        }

    }
}
