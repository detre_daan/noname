﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGuiTool : MonoBehaviour
{
    [SerializeField] GameObject[] tools;

    GameObject player;
    GetAndChangeActiveTool getAndChangeActiveTool;

    private void Start()
    {
        player = GameObject.Find("Player");
        getAndChangeActiveTool = player.GetComponent<GetAndChangeActiveTool>();
    }

    private void Update()
    {
        if (getAndChangeActiveTool.toolUsingIndex == 0)
        {

            tools[0].GetComponent<SpriteRenderer>().color = Color.white;
            tools[1].GetComponent<SpriteRenderer>().color = Color.gray;
            tools[2].GetComponent<SpriteRenderer>().color = Color.gray;
        }
        else if (getAndChangeActiveTool.toolUsingIndex == 1)
        {
            tools[0].GetComponent<SpriteRenderer>().color = Color.gray;
            tools[1].GetComponent<SpriteRenderer>().color = Color.gray;
            tools[2].GetComponent<SpriteRenderer>().color = Color.white;

        }        
        else if (getAndChangeActiveTool.toolUsingIndex == 2)
        {
            tools[0].GetComponent<SpriteRenderer>().color = Color.gray;
            tools[1].GetComponent<SpriteRenderer>().color = Color.white;
            tools[2].GetComponent<SpriteRenderer>().color = Color.gray;
        }        


    }
}
