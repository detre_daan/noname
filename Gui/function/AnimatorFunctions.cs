﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorFunctions : MonoBehaviour
{
    #region Variables
    public bool disableOnce;

    MenuButtonController menuButtonController;
    #endregion

    private void Start()
    {
        menuButtonController = transform.parent.GetComponent<MenuButtonController>();
    }

    void PlaySound(AudioClip whichSound)
    {
        if (!disableOnce)
        {
            menuButtonController.clickAudio.PlayOneShot(whichSound);
        }
        else
        {
            disableOnce = false;
        }
    }
}
