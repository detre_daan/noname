﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioObject : MonoBehaviour
{
    #region Variables
    [Header("Resources")]
    [SerializeField] AudioClip[] woodHit = null;
    [SerializeField] AudioClip[] stoneHit = null;
    [SerializeField] AudioClip[] ironHit = null;
    [SerializeField] AudioClip[] diamondHit = null;

    [Header("Entities")]
    [SerializeField] AudioClip[] zombieDeath = null;
    [SerializeField] AudioClip[] playerDamage = null;
    #endregion
    private void PlayRandomClip(AudioClip[] audioArray)
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        int rnd = Random.Range(0, audioArray.Length -1);
        AudioSource.PlayClipAtPoint(audioArray[rnd], Camera.main.transform.position);

    }

    #region Resources
    public void WoodHit()
    {
        PlayRandomClip(woodHit);
    }
    public void StoneHit()
    {
        PlayRandomClip(stoneHit);
    }
    public void IronHit()
    {
        PlayRandomClip(ironHit);
    }
    public void DiamondHit()
    {
        PlayRandomClip(diamondHit);
    }
    #endregion

    #region Entities
    public void PlayerDamage()
    {
        PlayRandomClip(playerDamage);
    }

    public void ZombieDeath()
    {
        PlayRandomClip(zombieDeath);
    }

    #endregion
}
