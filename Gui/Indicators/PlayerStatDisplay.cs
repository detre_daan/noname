﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerStatDisplay : MonoBehaviour
{
    #region variables
    [Header("Player")]
    GameObject player = null;    
    
    [Header("Health")]
    [SerializeField] TextMeshProUGUI hitpointValue = null;
    [SerializeField] TextMeshProUGUI hungerValue = null;
    [SerializeField] TextMeshProUGUI thirstValue = null;
    [SerializeField] TextMeshProUGUI warmthValue = null;

    [Header("Resources")]
    [SerializeField] TextMeshProUGUI woodValue = null;
    [SerializeField] TextMeshProUGUI stoneValue = null;
    [SerializeField] TextMeshProUGUI ironValue = null;
    [SerializeField] TextMeshProUGUI diamondValue = null;

    PlayerStatusClass playerStatus;

    #endregion

    void Start()
    {
        player = GameObject.Find("Player");
        playerStatus = player.GetComponent<PlayerStatusClass>();
    }


    void Update()
    {
        UpdateIndicators();
    }

    void UpdateIndicators()
    {
        //Takes different UI elements and updates those based on player stats
        hitpointValue.text = Mathf.Round(playerStatus.Hitpoints).ToString();
        hungerValue.text = Mathf.Round(playerStatus.Hunger).ToString();
        thirstValue.text = Mathf.Round(playerStatus.Thirst).ToString();
        warmthValue.text = Mathf.Round(playerStatus.BodyWarmth).ToString();

        woodValue.text = Mathf.Round(playerStatus.Wood).ToString();
        stoneValue.text = Mathf.Round(playerStatus.Stone).ToString();
        ironValue.text = Mathf.Round(playerStatus.Iron).ToString();
        diamondValue.text = Mathf.Round(playerStatus.Diamond).ToString();
    }

}
