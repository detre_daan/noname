﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMessage : MonoBehaviour
{
    #region Variables
    [SerializeField] TextMeshProUGUI TMPRO = null;
    [SerializeField] Transform playerCanvas = null;
    #endregion

    public void TriggerMessage(string message)
    {
        TMPRO.text = message;
        var varErrorMessage = Instantiate(TMPRO, new Vector3(transform.position.x, 
            transform.position.y + 25, transform.position.z), Quaternion.identity, playerCanvas);
    }
}
