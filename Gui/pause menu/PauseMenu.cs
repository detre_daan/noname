﻿using Pathfinding;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    #region Variables
    [SerializeField] GameObject pauseMenuUI = null;
    [SerializeField] GameObject optionsCanvas = null;

    bool gameIsPaused = false;
    #endregion

    void Update()
    {
        CheckPauseInput();
    }

    private void CheckPauseInput()
    {
        //Opens and closes pause menu when pressing submit
        if (Input.GetButtonDown("Submit"))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void SetPauseMenuInactive()
    {
        pauseMenuUI.SetActive(false);
    }    
    public void SetPauseMenuActive()
    {
        pauseMenuUI.SetActive(true);
    }

    public void Resume()
    {
        optionsCanvas.SetActive(false);
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }
    
    private void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }    
    public bool GameIsPaused
    {
        get { return gameIsPaused; }
        set { gameIsPaused = value; }
    }
}
