﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveLoadData : MonoBehaviour
{
    #region variables
    GameObject player;

    PlayerStatusClass playerStatus;
    string resourcePath = "Assets/Data/ResourceAndHealth.txt";
    string researchPath = "Assets/Data/ResearchData.txt";
    #endregion

    //Reads all the stats of the player and loads these when the scene gets loaded
    private void Awake()
    {
        player = GameObject.Find("Player");
        playerStatus = player.GetComponent<PlayerStatusClass>();
        LoadResourceData();
    }

    public void SaveResourceData()
    {
        StreamWriter sw = new StreamWriter(resourcePath);
        sw.WriteLine(playerStatus.ResourceSaveLine());
        sw.Close();
    }

    public void LoadResourceData()
    {
        StreamReader sr = new StreamReader(resourcePath);
        string[] resourceData = sr.ReadToEnd().Split(';');

        playerStatus.AddAllValues(float.Parse(resourceData[0]), float.Parse(resourceData[1]),
             float.Parse(resourceData[2]), float.Parse(resourceData[3]), float.Parse(resourceData[4]),
             float.Parse(resourceData[5]), float.Parse(resourceData[6]), float.Parse(resourceData[7]));
        sr.Close();
    }

    public bool[] LoadResearchData()
    {
        StreamReader sr = new StreamReader(researchPath);

        List<bool> researchUnlockedData = new List<bool>();

        foreach (var item in sr.ReadToEnd().Split(';'))
        {
            researchUnlockedData.Add(bool.Parse(item));
        }

        sr.Close();
        return researchUnlockedData.ToArray();
    }

    public void SaveResearchData(bool[] saveText)
    {
        StreamWriter sw = new StreamWriter(researchPath);
        string text = saveText[0].ToString();

        sw.Write(text);
        sw.Close();
    }
}
